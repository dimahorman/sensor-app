package service

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*

class ApiService {
    private val client = HttpClient(CIO)
    private val sensorId = SensorConfig.getSensorId()
    private val url = SensorConfig.getFunctionUrl()

    suspend fun pushMeasurements(value: String): String {
        return client.get("$url/FetchData?sensorId=$sensorId&value=$value")
    }
}
