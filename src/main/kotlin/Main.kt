import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import service.ApiService

fun main(args: Array<String>) = runBlocking {
   val apiService = ApiService()
    val job = launch {
        while (true) {
            val value = (0..100).random().toString()
            print("value = $value ")
            println(apiService.pushMeasurements(value))
            delay(10000)
        }
    }
}
