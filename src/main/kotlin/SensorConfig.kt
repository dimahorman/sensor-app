import org.example.util.Secrets

object SensorConfig {
    fun getSensorId(): String {
        return Secrets.get("SENSOR_ID")?: throw RuntimeException("Please define SENSOR_ID environment variable")
    }

    fun getFunctionUrl(): String {
        return Secrets.get("FUNCTION_URL")?: throw RuntimeException("Please define FUNCTION_URL environment variable")
    }
}
